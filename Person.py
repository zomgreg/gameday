class Person():

    def __init__(self):
        self.legs = 2

    def tip_amount(self):
        print 5


class RichPerson(Person):

    def tip_amount(self):
        print 10

p = Person()

p.tip_amount()

print p.legs

r = RichPerson()

r.tip_amount()
print r.legs