#!/usr/bin/env python
import datetime
import bs4
from xml.etree import ElementTree
import requests
import re
import os
import argparse
import collections
from players import players

MLB_BASE = '/Users/moselleg/mlb/'

def pitches_seen():
    pitches_seen = []
    for file in os.listdir(MLB_BASE):
        try:
            with open(MLB_BASE+file) as f:
                tree = ElementTree.parse(f)

                topbat = tree.findall('inning/top/atbat')
                bottombat = tree.findall('inning/bottom/atbat')

                for t in topbat:
                    pitch_count = 0
                    for g in t.getchildren():
                        if g.tag == 'pitch':
                            pitch_count += 1

                    pitches_seen.append(pitch_count)

                for b in bottombat:
                    pitch_count = 0
                    for g in t.getchildren():
                        if g.tag == 'pitch':
                            pitch_count += 1

                    pitches_seen.append(pitch_count)

        except ElementTree.ParseError, e:
            os.remove(MLB_BASE+file)

    pitches = collections.Counter(pitches_seen)

    print 'Pitches Seen Count'
    count = 0
    for p in pitches.most_common(20):
        count+=p[1]
        print '{:12} {:5}'.format(p[0],p[1])

    print '\nPitches Analyzed: {}'.format(count)



def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--player', '-p', help='Player ID. Ex: 621439')
    cmd_args = parser.parse_args()

    player = cmd_args.player

    #player = '621439'
    pitches_seen()

if __name__ == '__main__':
    main()
