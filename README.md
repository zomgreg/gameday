# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Working with the MLB Gameday data. Starting out as a collection of scripts

### Links

* http://gd2.mlb.com/components/game/mlb/year_2017/month_04/
* http://gd2.mlb.com/components/game/mlb/year_2017/month_04/day_04/gid_2017_04_04_detmlb_chamlb_1/inning/inning_1.xml
* http://gd2.mlb.com/components/game/mlb/year_2017/month_04/day_04/gid_2017_04_04_detmlb_chamlb_1/boxscore.json
* http://gd2.mlb.com/components/game/mlb/year_2017/month_04/day_06/gid_2017_04_06_kcamlb_minmlb_1/miniscoreboard.xml
* http://gd2.mlb.com/components/game/mlb/year_2017/month_04/day_06/gid_2017_04_06_kcamlb_minmlb_1/game_events.json
* http://gd2.mlb.com/components/game/mlb/year_2017/month_04/day_06/gid_2017_04_06_kcamlb_minmlb_1/players.xml
* http://gd2.mlb.com/components/game/mlb/year_2017/month_04/day_06/gid_2017_04_06_kcamlb_minmlb_1/eventLog.xml
* http://gd2.mlb.com/components/game/mlb/year_2017/month_04/day_06/gid_2017_04_06_kcamlb_minmlb_1/linescore.json
* https://fastballs.wordpress.com/category/pitchfx-glossary/