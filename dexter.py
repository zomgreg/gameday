import collections
import seaborn as sns
import matplotlib.pyplot as plt
import os

BASE = '/Users/moselleg/dexter_files'
RESPONSE_CODES = []

for file in os.listdir(BASE):
    if file.endswith('.txt'):
        f = open(BASE + '/' + file, 'r')

        lines = f.readlines()
        slice = []

        for i, j in enumerate(lines):
            if j.startswith('%#INTERNET'):
                slice.append(i)

        for i in lines[slice[0]:slice[1]]:
            if i.startswith('HTTP'):
                RESPONSE_CODES.append(i.split()[1].replace(':', ''))

rc = collections.Counter(RESPONSE_CODES)

print rc.most_common(20)

codes = [i[0] for i in rc.most_common(20)]
count = [i[1] for i in rc.most_common(20)]

total = len(RESPONSE_CODES)

three = len([i for i in RESPONSE_CODES if int(i) < 400])
five = len([i for i in RESPONSE_CODES if int(i) >= 500])
four = total - (three + five)

# Something is wrong with this counting of types
print '3XX: {} ({}%), 4XX: {}, 5XX: {}'.format(three, four, five, three / total)

sns.set_style("whitegrid")
colors = sns.color_palette("Spectral", 10)

fig, ax = plt.subplots()
sns_plot = sns.barplot(codes, count, palette=colors)

for p in sns_plot.patches:
    sns_plot.annotate(
        s='  {},{}%'.format(int(p.get_height()), round(p.get_height() * 100 / total, 2)),
        xy=(p.get_x() + p.get_width() / 2., p.get_height()),
        ha='center', va='center',
        xytext=(0, 10),
        size=8,
        textcoords='offset points'
    )

sns_plot.set(xlabel='HTTP Code', ylabel='Count')
sns.plt.title('HTTP Response Codes')

sns_plot.figure.savefig('/tmp/dexter.png')
