#!/usr/bin/env python
import datetime
import bs4
from xml.etree import ElementTree
import requests
import re
import os
import argparse
import collections
from players import players


class Update():

    def __init__(self, **kwargs):
        self.start_date = datetime.date(2017, 06, 01)
        self.end_date = datetime.date(2017, 07, 01)
        self.today = datetime.date.today()
        self.url = 'http://gd2.mlb.com/components/game/mlb/year_{}/month_{:02d}/day_{:02d}/'.format(self.start_date.year,
                                                                                                    self.start_date.month,
                                                                                                    self.start_date.day)
        self.path = '/Users/moselleg/projects/gameday/source/data/'

    def clean_files(self):
        for file in os.listdir(self.path):
            if os.path.getsize(self.path+file) < 1000:
                os.remove(self.path+file)

    def update_files(self):

        inning_games = []

        while self.start_date < self.today:
            self.start_date += datetime.timedelta(days=1)

            result = requests.get(self.url)
            print result.status_code
            c = result.content
            soup = bs4.BeautifulSoup(c, "html.parser")

            links = soup.find_all('a', href=re.compile('^gid'))
            for l in links:
                filename = str(l.contents[0].replace('/',''))
                game = str(l.contents[0])
                game_url = self.url+game.strip()
                inning_games.append(game_url+'inning/inning_all.xml')

                if not os.path.isfile('/Users/moselleg/mlb/'+filename.strip()):
                    with open(self.path+filename.strip()+'.xml','wb') as f:
                        f.write(requests.get(game_url+'inning/inning_all.xml').content)


class ParseInnings():

    def __init__(self, **kwargs):

        self.MLB_BASE = '/Users/moselleg/mlb/'
        self.player = kwargs.get('player', '621439')

    def parse_innings(self):
        results = []
        pitches_seen = []
        for file in os.listdir(self.MLB_BASE):
            # if file == 'gid_2017_04_30_minmlb_kcamlb_1.xml':
            try:
                with open(self.MLB_BASE+file) as f:
                    tree = ElementTree.parse(f)

                    topbat = tree.findall('inning/top/atbat')
                    bottombat = tree.findall('inning/bottom/atbat')

                    for t in topbat:
                        pitch_count = 0
                        if t.attrib['batter'] == self.player:
                            results.append(t.attrib['event'])

                            for g in t.getchildren():
                                if g.tag == 'pitch':
                                    pitch_count += 1

                            pitches_seen.append(pitch_count)


                    for b in bottombat:
                        pitch_count = 0
                        if b.attrib['batter'] == self.player:
                            results.append(b.attrib['event'])

                            for g in b.getchildren():
                                if g.tag == 'pitch':
                                    pitch_count += 1

                            pitches_seen.append(pitch_count)


            except (ElementTree.ParseError), e:
                os.remove(self.MLB_BASE+file)

        rc = collections.Counter(results)
        pitches = collections.Counter(pitches_seen)

        print rc.most_common(20)
        print pitches.most_common(20)


def main():

    u = Update()
    # u.update_files()
    u.clean_files()

    # p = ParseInnings(player='621439')
    # p.parse_innings()

    #
    # parser = argparse.ArgumentParser()
    # parser.add_argument('--player', '-p', help='Player ID. Ex: 621439')
    # cmd_args = parser.parse_args()
    #
    # player = cmd_args.player
    #
    # player = '621439'
    # parse_innings(player)

if __name__ == '__main__':

    main()
