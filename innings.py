import requests
import collections
from xml.etree import ElementTree
import lut


# url = 'http://gd2.mlb.com/components/game/mlb/year_2017/month_04/day_14/gid_2017_04_14_chamlb_minmlb_1/inning/inning_all.xml'
# url = 'http://gd2.mlb.com/components/game/mlb/year_2017/month_04/day_15/gid_2017_04_15_chamlb_minmlb_1/inning/inning_all.xml'
# r = requests.get(url)

# with open("/tmp/innings.xml", "wb") as innings:
#     innings.write(r.content)

with open('/tmp/innings.xml', 'rt') as f:
    tree = ElementTree.parse(f)

for node in tree.iter('inning'):
    if node.attrib.get('num') == '1':
        away_team = node.attrib.get('away_team')
        home_team = node.attrib.get('home_team')

print '\nAway Team: {}'.format(lut.TEAM[away_team])
print 'Home Team: {}\n'.format(lut.TEAM[home_team])

away_pitches = []
home_pitches = []


for node in tree.iter('top'):
    for a in node.iter('atbat'):
        for b in a.iter('pitch'):
            away_pitches.append(b.attrib.get('pitch_type'))

for node in tree.iter('bottom'):
    for a in node.iter('atbat'):
        for b in a.iter('pitch'):
           home_pitches.append(b.attrib.get('pitch_type'))

away_collection = collections.Counter(away_pitches)
home_collection = collections.Counter(home_pitches)

total_away = len(away_pitches)
total_home = len(home_pitches)

print '{} Summary'.format(lut.TEAM[away_team])
for a in away_collection.most_common(20):
    print '{:18} {:3} {} %'.format(lut.PITCHES[a[0]], a[1], round(float(a[1])*100/float(total_away),2))

print '-'*20

print '{} Summary'.format(lut.TEAM[home_team])
for a in home_collection.most_common(20):
    print '{:18} {:3} {} %'.format(lut.PITCHES[a[0]], a[1], round(float(a[1])*100/float(total_home),2))

# pitch_types = [node.attrib.get('pitch_type') for node in tree.iter('pitch') if node.attrib.get('pitch_type') != None]
#
# total_pitches=len(pitch_types)
#
# c = collections.Counter(pitch_types)
#
# for c in c.most_common(20):
#     print '{:18} {:3} {} %'.format(lut.PITCHES[c[0]], c[1], round(float(c[1])*100/float(total_pitches),2))
#
# print '{}'.format(29*'-')
# print '{:18} {:3}'.format('Total',total_pitches)