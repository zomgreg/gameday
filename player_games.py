#!/usr/bin/env python

import datetime
import xmltodict
import json
import bs4
from xml.etree import ElementTree
import requests
import re
import os
import argparse
import collections
from players import players

MLB_BASE = './source/data/'
PLAYER = '621439'

at_bats = []

class Game():

    def __init__(self, **kwargs):
        self.player = kwargs.get('player', '621439')
        self.limit = kwargs.get('limit', 10)
        self.file = kwargs.get('file', file)
        self.atbats = []
        self.pitches = {}

    def get_pitches(self):

        at_bats = self.get_atbats()

        if at_bats is not None:
            for a in at_bats:
                this_atbat = {'result': a['@event'], 'atbat_datetime': a['@start_tfs_zulu']}
                pitches = []
                try:
                    if len(a['pitch']) == 40:
                        pitch = {}
                        pitch['datetime'] = p['@tfs_zulu']
                        pitch['date'] = p['@tfs_zulu'][:10]
                        pitch['start_speed'] = p['@start_speed']
                        pitch['end_speed'] = p['@end_speed']
                        pitch['nasty'] = p['@nasty']
                        pitch['pitch_type'] = p['@pitch_type']
                        pitch['type'] = p['@type']

                        pitches.append(pitch)

                        this_atbat['pitches'] = pitches
                except (KeyError, TypeError):
                    pass

                else:
                    for p in a['pitch']:
                        try:
                            pitch = {}
                            pitch['datetime'] = p['@tfs_zulu']
                            pitch['date'] = p['@tfs_zulu'][:10]
                            pitch['start_speed'] = p['@start_speed']
                            pitch['end_speed'] = p['@end_speed']
                            pitch['nasty'] = p['@nasty']
                            pitch['pitch_type'] = p['@pitch_type']
                            pitch['type'] = p['@type']

                            pitches.append(pitch)

                            this_atbat['pitches'] = pitches
                        except (TypeError, KeyError):
                            pass

                self.atbats.append(this_atbat)
            return self.atbats
                        # print '{} {} {} {} {} {} {}'.format(p['@tfs_zulu'][:10], p['@tfs'], p['@start_speed'], p['@end_speed'], p['@nasty'], p['@pitch_type'], p['@type'])


    def get_atbats(self):
        '''
        Have to check if there is a bottom of the inning.
        :return:
        '''

        at_bats = []
        innings = self._get_innings()

        for i in innings:
            try:
                if 'bottom' in i:
                    for b in i['bottom']['atbat']:
                        if b['@batter'] == self.player:
                            at_bats.append(b)
                for t in i['top']['atbat']:
                    if t['@batter'] == self.player:
                        at_bats.append(t)
            except TypeError:
                pass

        if len(at_bats) > 1:
            return at_bats

    def _get_innings(self):
        try:
            with open(self.file) as f:
                game_dict = xmltodict.parse(f.read())
        except (AttributeError, TypeError, KeyError), e:
            print e

        return game_dict['game']['inning']

    def get_teams(self):
        try:
            with open(self.file) as f:
                game_dict = xmltodict.parse(f.read())

                for g in game_dict['game']['inning']:
                    if isinstance(g, collections.OrderedDict):
                        print g['@away_team']

        except (AttributeError, TypeError, KeyError), e:
            print e

count=0

strikeout_count = 0

for file in os.listdir(MLB_BASE):

    limit = 4000

    if count < limit:
        count += 1

        file = os.path.abspath(MLB_BASE+file)

        g = Game(player='621439', limit=10, file=file, base_url=MLB_BASE)
        # game = g.get_atbats()
        game = g.get_pitches()
        if game is not None:
            for g in game:
                # print g['result'], g['atbat_datetime']
                if g['result'] == 'Strikeout':
                    strikeout_count += 1

print strikeout_count
                # for p in g['pitches']:
                #     print p['datetime'], p['pitch_type']

        # print g.get_num_innings(game)




#byron = PlayerAtBats(player='621439', limit=1000)


    # def get_atbats(self):
    #     count = 0
    #     pitch_types=[]
    #     print '{},{},{},{},{},{},{}'.format('DateTime','Time', 'Start Speed','Pitch Type','Confidence','Nasty','Result')
    #     for file in os.listdir(MLB_BASE):
    #         # print file
    #         # if file == 'gid_2017_04_30_minmlb_kcamlb_1.xml':
    #         # if file == 'gid_2017_06_08_slnmlb_cinmlb_1.xml':
    #         # if file == 'gid_2017_04_03_phimlb_cinmlb_1.xml' or  file == 'gid_2017_04_15_balmlb_tormlb_1.xml':
    #         if count < self.limit:
    #             try:
    #                 with open(MLB_BASE+file) as f:

                        # game_dict = xmltodict.parse(f.read())

                        # for i in game_dict['game']['inning']:
                        #     for k, v in i.iteritems():
                        #         if k == 'top' or k == 'bottom':
                        #             for l, m in v.iteritems():
                        #                 if l == 'atbat':
                        #                     for o in m:
                        #                         if o['@batter'] == self.player:
                        #                             # print 'found {}'.format(players[self.player])
                        #                             description = o['@des']
                        #                             for k, v in o.iteritems():
                        #                                 if k == 'pitch':
                        #                                     if isinstance(v, list):
                        #                                         pitches = len(v)
                        #                                         for p in v:
                        #                                             # print '{},{},{},{},{},{},{}'.format(p['@tfs_zulu'], p['@tfs'], p['@start_speed'], p['@pitch_type'], p['@type_confidence'], p['@nasty'], str(p['@des']).replace(',',''))
                        #                                             pitch_types.append(str(p['@pitch_type']))
                        #                                     else:
                        #                                         # print '{},{},{},{},{},{},{}'.format(v['@tfs_zulu'], v['@tfs'], v['@start_speed'], v['@pitch_type'], v['@type_confidence'], v['@nasty'], v['@des'].replace(',',''))
                        #                                         pitch_types.append(str(p['@pitch_type']))
                        #                             # print '{} on {} pitches'.format(description,pitches)
                        #                             count += 1

                # except (AttributeError, TypeError, KeyError), e:
                #     pass
                #     # print 'Error in file {}'.format(file)
                #     # os.remove(MLB_BASE+file)

        # return pitch_types

#byron = PlayerAtBats(player='621439', limit=1000)
#byron_pitches = byron.get_atbats()
#byron_collection = collections.Counter(byron_pitches)
#print byron_collection.most_common(20)
#
#bryce = PlayerAtBats(player='547180', limit=1000)
#bryce_pitches = bryce.get_atbats()
#bryce_collection = collections.Counter(bryce_pitches)
#print bryce_collection.most_common(20)
