TEAM = {'kca': 'Kansas City Royals',
        'min': 'Minnesota Twins',
        'cha': 'Chicago White Sox'}

PITCHES = {
    'FA': 'Fastball',
    'FT': 'Two-seam fastball',
    'FF': 'Four-seam fastball',
    'SI': 'Sinker',
    'CH': 'Change-up',
    'SL': 'Slider',
    'CU': 'Curveball',
    'FC': 'Cut fastball',
    'FS': 'Split-finger fastball',
    'None': 'None',
    'KN': 'Knuckleball'
}